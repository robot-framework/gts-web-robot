*** Settings ***
Resource          ../resources/imports.robot

*** Keywords ***
Open headless browser
    ${c_opts} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${c_opts}   add_argument    disable-gpu
    Call Method    ${c_opts}   add_argument    no-sandbox
    Call Method    ${c_opts}   add_argument    window-size\=1024,768
    Call Method    ${c_opts}   add_argument    User-agent: Googlebot-Image
    Create Webdriver    Chrome    crm_alias    chrome_options=${c_opts}
