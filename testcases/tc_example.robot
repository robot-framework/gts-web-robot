*** Settings ***
Documentation     Test Suite Description
Default Tags      tag_name_1    tag_name_2
Resource          ../../keywords/common_keywords.robot
Resource          ../../keywords/your_keywords.robot
Suite Setup       Run Keywords  Connect Database
...               Connect To Redis Server
...               Open Connection SSH
Suite Teardown    Run Keywords  Disconnect from Database
...               Close Connection SSH
...               Disconnect To Redis Server

*** Test Cases ***
TC_NAME_01
    [Documentation]  [Module name] Description your test case
    [Setup]  Run keywords  Set 'data' Property Is '0' into file config
    ...    Set prepair data 1
    ...    Set prepair data 2
    Given Add condition 1
    And Add condition 2
    And Add condition 3
    When Do your task
    Then Check data resutl
    And Pass condtion 1
    And Pass condtion 2
    [Teardown]  Reset 'data' Property Default Value Into File Config